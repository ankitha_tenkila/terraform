module "vpc" {
  source = "./vpc"
  cidrblock= "10.0.0.0/16"
  subnetcidr= "10.0.1.0/24"
  externalip="0.0.0.0/0"
  
}


module "ec2" {
  source = "./web"
  sn= module.vpc.subnetval
  sg= module.vpc.securitygroupval
}

